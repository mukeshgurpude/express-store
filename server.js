import express from 'express';
import session from 'express-session';
import cors from 'cors';
import { config } from 'dotenv';
import { connectToServer, getDB } from './config/db.js';
import path from 'path';
import User from './routes/user.js'
import Product from './routes/products.js'
import Payment from './routes/payment.js'

config();
const app = express();
const __dirname = path.resolve();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(session({
  secret: process.env.SECRET || 'secret',
  saveUninitialized: true,
  resave: true
}))
app.use('/public', express.static(path.join(__dirname, 'public')));

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

app.use('', User)
app.use('/products', Product)
app.use('', Payment)

app.get('/', (req, res) => {
  const db = getDB();
  db.collection('products').find().toArray()
  .then((products) => {
    res.render('index', { products });
  })
});


connectToServer(function(err) {
  if(err) {
    console.log('Unable to connect to database')
    process.exit(1)
  }
  app.listen(process.env.PORT, () => {
    console.log(`Server is running on port ${process.env.PORT}`);
  })
})
