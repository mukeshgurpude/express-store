A Store with express-js
---

## Running Locally:
1. Install node.js

2. Install npm

3. Clone the repository
    ```console
    git clone git@gitlab.com/mukeshgurpude/express-store
    ```

4. Install dependencies
    ```console
    npm install
    ```

5. Configure environment variables
    ```console
    cp .env.example .env
    ```
    the correct values

6. Run the express server
    ```console
    npm start
    ```
---

## Routes
  Endpoint| Description
  --- | ---
  `/login` | Login
  `/register` | Register new User
  `/products/orders` | View all orders
  ---

## Tech Stack
  - Express js
  - Pug Templates
  - Stripe API
