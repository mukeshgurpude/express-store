import { getDB } from '../config/db.js'
import { Router } from 'express'
import { config } from 'dotenv'
import { ObjectId } from 'mongodb'
import Stripe from 'stripe'

config()
const Product = Router()
const stripe = Stripe(process.env.STRIPE_SECRET_KEY)

function ensureAuthenticated(req, res, next) {
  if (req.session.user) {
    return next()
  }
  req.session.error = 'You\'re not logged in!'
  res.redirect('/login')
}

Product.post('/:id/buy', ensureAuthenticated, (req, res) => {
  const { id } = req.params
  const db = getDB()
  let port = 80;
  if(req.hostname === 'localhost') port = process.env.PORT;
  const base_url = `${req.protocol}://${req.hostname}:${port}`

  db.collection('products').findOne({ _id: new ObjectId(id) })
  .then(product => {
    if(!product) {
      throw new Error('Product not found!')
    }
    db.collection('orders').insertOne({
      user: req.session.user,
      product: new ObjectId(id),
      status: 'pending',
      created_at: new Date()
    }).then(result => {
      // Create a checkout session
      stripe.checkout.sessions.create({
        client_reference_id: result.insertedId,
        payment_method_types: ['card'],
        line_items: [{
          name: product.name,
          images: [product.image],
          amount: product.price*100,
          currency: 'inr',
          quantity: 1
        }],
        success_url: base_url + '/products/orders',
        cancel_url: base_url + '/products/orders'
      }).then(session => {
        db.collection('orders').updateOne({ _id: result.insertedId }, { $set: {
          session_id: session.id,
          payment_intent: session.payment_intent
        }})
        res.redirect(session.url)
      })
    }).catch(err => {
      console.log(err)
      res.redirect('/')
    })
  })
  .catch(err => {
    console.log(err)
    res.redirect('/')
  })
})

Product.get('/orders', ensureAuthenticated, (req, res) => {
  const db = getDB()
  db.collection('orders').aggregate(
    [{$lookup: {from: "products", localField: "product", foreignField: "_id", as: "product"}}],
    {$unwind: "$product"}
  ).match({user: req.session.user})
  .project({
    "_id": 0,
    id: "$_id",
    name: "$product.name",
    price: "$product.price",
    image: "$product.image",
    created_at: 1,
    payment_intent: 1,
    status: 1
  }).toArray()
  .then(async orders => {
    await Promise.all(orders.map(async (order, index) => {
      if(order.status === 'pending') {
        // Update order status, if it is still pending
        const paymentIntent = await stripe.paymentIntents.retrieve(order.payment_intent)
        if(paymentIntent.status === 'succeeded') {
          db.collection('orders').updateOne({ _id: order.id }, { $set: { status: 'success' }})
          orders[index].status = 'success'
        }else if(['requires_payment_method', 'payment_failed', 'cancelled'].includes(paymentIntent.status)) {
          db.collection('orders').updateOne({ _id: order.id }, { $set: { status: 'cancelled' }})
          orders[index].status = 'cancelled'
        }
      }
    }))
    res.render('orders', { orders })
  })
})

export default Product;
