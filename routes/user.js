import { Router } from "express";
import { getDB } from "../config/db.js";
import crypto from "crypto";

const User = Router()


function sha256(password) {
  return crypto.createHash("sha256").update(password).digest("hex");
}

User.route('/login').get((req, res) => {
  if (req.session.user) {
    res.redirect('/');
  } else {
    let error = req.session.error;
    req.session.error = null;
    res.render('login', {error});
  }
}).post((req, res) => {
  const db = getDB();
  db.collection('users').findOne({
    username: req.body.username,
    password: sha256(req.body.password)
  }).then((user) => {
    if (user) {
      req.session.user = user.username;
      res.redirect('/');
    } else {
      req.session.error = 'Invalid username or password';
      res.redirect('/login');
    }
  });
});

User.route('/register').get((req, res) => {
  if (req.session.user) {
    res.redirect('/');
  } else {
    let error = req.session.error;
    req.session.error = null;
    res.render('register', {error});
  }
}).post((req, res) => {
  const db = getDB();
  db.collection('users').findOne({
    username: req.body.username
  }).then((user) => {
    if (user) {
      req.session.error = 'Username already exists, please login';
      res.redirect('/login');
    } else {
      db.collection('users').insertOne({
        username: req.body.username,
        password: sha256(req.body.password)
      }).then(() => {
        res.redirect('/login');
      });
    }
  });
});

export default User;
