import { MongoClient } from "mongodb";
import { config } from "dotenv";

config();

const client = new MongoClient(process.env.DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

let db;

export async function connectToServer(callback) {
  client.connect(function(err, connection) {
    if(err) return callback(err);
    db = connection.db('express-store');
    console.log('Connected to MongoDB');
    return callback();
  })
}

export function getDB() {
  return db;
}
